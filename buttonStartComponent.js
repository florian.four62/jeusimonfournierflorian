Vue.component('button-start', {
    data: function () {
      return {
          playText : "Play"
      }
  },
  template: `<button v-on:click="$emit('click')">{{playText}}</button>`,
  
})