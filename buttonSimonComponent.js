Vue.component('button-simon', {
    props:['button'],
    template : `
         <span class="carre" 
         v-bind:id="button.color" 
         v-on:click="$emit('click', button.color)" 
         v-bind:style="{ backgroundColor : button.actif ? button.backgroundColor : '' }">
         {{button.color}}
         </span>`,
    methods : {
        
    }

})